package com.gobelins.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gobelins.news.model.Article
import com.gobelins.news.repository.ArticleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch

class ArticlesViewModel : ViewModel() {

    private val repository: ArticleRepository = ArticleRepository()
    private val _listArticles = MutableLiveData<List<Article>>()

    val listArticles: LiveData<List<Article>>
        get() = _listArticles

    fun loadData() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getArticles()
            _listArticles.postValue(result)
        }

        /*object : Thread() {
            override fun run() {
                val result = repository.getArticles()
                _listArticles.postValue(result)
            }
        }.start()*/
    }
}