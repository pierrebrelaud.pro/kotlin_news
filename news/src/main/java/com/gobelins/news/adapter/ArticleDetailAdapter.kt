package com.gobelins.news.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.gobelins.news.fragment.ArticleDetailFragment
import com.gobelins.news.fragment.article_details.ArticleDetailContentFragment
import com.gobelins.news.fragment.article_details.ArticleDetailWebFragment
import com.gobelins.news.model.Article

class ArticleDetailAdapter(fm: FragmentManager, val article: Article) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> {
                Log.i("article", "article : ${article.title}")
                ArticleDetailContentFragment.newInstance(article)
            }
            1 -> ArticleDetailWebFragment.newInstance(article)
            else -> ArticleDetailContentFragment.newInstance(article)
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> "Article"
            1 -> "Web"
            else -> "Article"
        }
    }

}