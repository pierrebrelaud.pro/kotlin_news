package com.gobelins.news.fragment.article_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.gobelins.news.R
import com.gobelins.news.adapter.ArticleDetailAdapter
import com.gobelins.news.fragment.ArticleDetailFragment
import com.gobelins.news.model.Article
import kotlinx.android.synthetic.main.article_detail_web_fragment.*
import kotlinx.android.synthetic.main.articles_detail_fragment.*


class ArticleDetailWebFragment : Fragment() {

    private val article: Article by lazy {
        arguments?.getParcelable<Article>(ArticleDetailWebFragment.ARG_ARTICLE)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.article_detail_web_fragment, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val context = context ?: return
        super.onViewCreated(view, savedInstanceState)

        article_detail_web.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        article_detail_web.loadUrl(article.url)
    }
    companion object {

        private const val ARG_ARTICLE = "ARG_ARTICLE"

        fun newInstance(article: Article): ArticleDetailWebFragment {
            return ArticleDetailWebFragment().apply {
                arguments = bundleOf(ARG_ARTICLE to article)
            }
        }
    }
}