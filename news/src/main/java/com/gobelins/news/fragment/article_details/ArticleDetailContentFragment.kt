package com.gobelins.news.fragment.article_details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.gobelins.news.R
import com.gobelins.news.model.Article
import kotlinx.android.synthetic.main.article_detail_content_fragment.*

class ArticleDetailContentFragment : Fragment() {
    private val article: Article by lazy {
        arguments?.getParcelable<Article>(ArticleDetailContentFragment.ARG_ARTICLE)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val context = context ?: return
        super.onViewCreated(view, savedInstanceState)

        article_detail_title.text = article.title
        article_detail_content.text = article.content
        article_detail_source.text = article.source.name
        Glide.with(context).load(article.image).into(article_detail_image)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.article_detail_content_fragment, container, false)
    }

    companion object {

        private const val ARG_ARTICLE = "ARG_ARTICLE"

        fun newInstance(article: Article): ArticleDetailContentFragment {
            Log.i("article", "article : ${article.title}")
            return ArticleDetailContentFragment().apply {
                arguments = bundleOf(ARG_ARTICLE to article)
            }
        }
    }
}
