package com.gobelins.news.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gobelins.news.R
import com.gobelins.news.adapter.ArticleAdapter
import com.gobelins.news.model.Article
import com.gobelins.news.viewmodel.ArticlesViewModel
import kotlinx.android.synthetic.main.articles_list_fragment.*

class ArticleListFragment : Fragment() {

    lateinit var articlesViewModel: ArticlesViewModel
    private lateinit var articleAdapter: ArticleAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.articles_list_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        articleAdapter = ArticleAdapter {
            displayArticleDetails(article = it)
        }
        articlesViewModel = ViewModelProvider(this).get(ArticlesViewModel::class.java)
        articlesViewModel.loadData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        articles_recycler_view.layoutManager = LinearLayoutManager(context)
        articles_recycler_view.adapter = articleAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        articlesViewModel.listArticles.observe(viewLifecycleOwner, Observer {
            articleAdapter.updateData(it)
        })
    }

    private fun displayArticleDetails(article: Article) {

        Log.i("article", "article adapter : ${article.title}")
        val articleDetailFragment = ArticleDetailFragment.newInstance(article)

        activity!!.supportFragmentManager.beginTransaction().apply {
            replace(R.id.home_fragment_container, articleDetailFragment)
            addToBackStack(null)
        }.commit()
    }
}