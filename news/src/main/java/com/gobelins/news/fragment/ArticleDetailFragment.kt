package com.gobelins.news.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.gobelins.news.R
import com.gobelins.news.adapter.ArticleDetailAdapter
import com.gobelins.news.model.Article
import kotlinx.android.synthetic.main.articles_detail_fragment.*

class ArticleDetailFragment : Fragment() {

    private lateinit var articleDetailAdapter: ArticleDetailAdapter

    private val article: Article by lazy {
        arguments?.getParcelable<Article>(ARG_ARTICLE)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.articles_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = context ?: return
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.hide()
        val articleDetailAdapter = ArticleDetailAdapter(fragmentManager!!, article)
        Log.i("article", "article view created: ${article.title}")
        article_detail_viewpager.adapter = articleDetailAdapter
        article_detail_tablayout.setupWithViewPager(article_detail_viewpager)
    }

    companion object {

        private const val ARG_ARTICLE = "ARG_ARTICLE"

        fun newInstance(article: Article): ArticleDetailFragment {
            return ArticleDetailFragment().apply {
                arguments = bundleOf(ARG_ARTICLE to article)
            }
        }
    }
}