package com.gobelins.news

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gobelins.news.fragment.ArticleListFragment

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        val articlesFragment = ArticleListFragment()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.home_fragment_container, articlesFragment)
            addToBackStack(null)
        }.commit()
    }
}
