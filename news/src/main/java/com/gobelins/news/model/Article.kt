package com.gobelins.news.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Article(
    val author: String,
    val source: Source,
    val content: String,
    val description: String,
    @SerializedName("publishedAt")
    val date: String,
    val title: String,
    val url: String,
    @SerializedName("urlToImage")
    val image: String
) : Parcelable