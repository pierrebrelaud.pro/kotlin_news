package com.gobelins.news.model

data class ArticleApiResult (val status: String, val totalResults: Int, val articles: List<Article>)