package com.gobelins.news.repository

import com.gobelins.news.datatsource.RemoteDataSource
import com.gobelins.news.model.Article

class ArticleRepository {
    private val online = RemoteDataSource()
    private lateinit var articles: List<Article>

    fun getArticles(): List<Article> {

        if(!::articles.isInitialized) {
            articles = online.getRemoteArticles()
        }
        return articles
    }
}