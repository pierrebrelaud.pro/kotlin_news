package com.gobelins.news.service

import com.gobelins.news.model.ArticleApiResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleService {
    @GET("/v2/everything")
    fun getArticles(@Query("apiKey") apiKey: String, @Query("q") q: String): Call<ArticleApiResult>
}
//?q=trump&apiKey=60c4812c5e96413ea906908587b014db