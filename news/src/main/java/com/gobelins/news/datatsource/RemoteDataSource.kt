package com.gobelins.news.datatsource

import android.util.Log
import com.gobelins.news.BuildConfig
import com.gobelins.news.model.Article
import com.gobelins.news.service.ArticleService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RemoteDataSource {
    private val service: ArticleService

    init {
        val httpLoginInterceptor = HttpLoggingInterceptor()
        httpLoginInterceptor.level =
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        var client = OkHttpClient.Builder()
            .addInterceptor(httpLoginInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder().apply {
            //Ajouter un converter pour JSON
            //Ici on utilise gson
            addConverterFactory(GsonConverterFactory.create())
            client(client)
            //Ajouter l'url de base du web service
            baseUrl("https://newsapi.org")
        }.build()
        //Créer une instance du service
        service = retrofit.create(ArticleService::class.java)
    }

    fun getRemoteArticles(): List<Article> {
        val result = service.getArticles("60c4812c5e96413ea906908587b014db", "android").execute()
        return if (result.isSuccessful) {
            result.body()!!.articles
        } else {
            emptyList()
        }
    }
}