package com.gobelins.dmii

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import com.gobelins.dmii.fragments.ChoiceFragment
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class Operation : Parcelable {
    PLUS, MINUS, TIMES, DIVIDE
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val choiceFragment = ChoiceFragment()
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragment_container, choiceFragment)
                addToBackStack(null)
            }.commit()
        }

    }

}

