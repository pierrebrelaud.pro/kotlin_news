package com.gobelins.dmii.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gobelins.dmii.Operation
import com.gobelins.dmii.R
import com.gobelins.dmii.extensions.toDouble
import com.gobelins.dmii.viewmodels.ComputeViewModel
import kotlinx.android.synthetic.main.compute_fragment.*

class ComputeFragment : Fragment() {
    private val operation: Operation by lazy {
        arguments?.getParcelable(ARGS_OPERATION) ?: Operation.PLUS
    }

    private lateinit var viewModel: ComputeViewModel

    companion object {

        const val ARGS_OPERATION = "ARGS_OPERATION"

        fun newInstance(operation: Operation): ComputeFragment {
            return ComputeFragment().apply {
                arguments = bundleOf(ARGS_OPERATION to operation)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ComputeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.compute_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        number_2.addTextChangedListener(textWatcher)
        number_1.addTextChangedListener(textWatcher)


        btn_back.setOnClickListener {

            val choiceFragment = ChoiceFragment()

            activity!!.supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragment_container, choiceFragment)
                addToBackStack(null)
            }.commit()
        }
    }
    private fun compute() {

        val nb1 = number_1.toDouble() ?: return
        val nb2 = number_2.toDouble() ?: return

        viewModel.getOperation(
            operation = operation,
            nb1 = nb1,
            nb2 = nb2
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.resultLiveData.observe(viewLifecycleOwner, Observer {
            result.text = it.toString()
        })
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) { }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            compute()
        }
    }
}