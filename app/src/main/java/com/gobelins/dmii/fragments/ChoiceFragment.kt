package com.gobelins.dmii.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gobelins.dmii.Operation
import com.gobelins.dmii.R
import kotlinx.android.synthetic.main.choice_fragment.*

class ChoiceFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.choice_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_plus.setOnClickListener { displayCompute(Operation.PLUS) }
        btn_minus.setOnClickListener { displayCompute(Operation.MINUS) }
        btn_times.setOnClickListener { displayCompute(Operation.TIMES) }
        btn_divide.setOnClickListener { displayCompute(Operation.DIVIDE) }

        btn_display_coordinates.setOnClickListener { displayLocation() }
    }

    private fun displayLocation() {
        val locationFragment = LocationFragment()

        activity!!.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, locationFragment)
            addToBackStack(null)
        }.commit()
    }

    private fun displayCompute(operation: Operation) {
        val computeFragment = ComputeFragment.newInstance(operation)

        activity!!.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, computeFragment)
            addToBackStack(null)
        }.commit()
    }
}