package com.gobelins.dmii.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gobelins.dmii.Operation

class ComputeViewModel : ViewModel() {

    private val _resultLiveData = MutableLiveData<Double>()
    val resultLiveData: LiveData<Double>
        get() = _resultLiveData

    fun getOperation(operation: Operation, nb1: Double, nb2: Double) {
        _resultLiveData.value = when (operation) {
            Operation.PLUS -> nb1.plus(nb2)
            Operation.MINUS -> nb1.minus(nb2)
            Operation.TIMES -> nb1.times(nb2)
            Operation.DIVIDE -> nb1.div(nb2)
        }
    }
}
