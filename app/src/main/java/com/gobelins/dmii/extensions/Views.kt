package com.gobelins.dmii.extensions

import android.widget.EditText
import com.gobelins.dmii.R

fun EditText.toDouble() : Double? {
    return text.toString().toDoubleOrNull() ?: run {
        error = context.getString(R.string.to_double_error)
        null
    }
}